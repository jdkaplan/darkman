module gitlab.com/WhyNotHugo/darkman

go 1.16

require (
	github.com/adrg/xdg v0.3.3
	github.com/godbus/dbus/v5 v5.0.4
	github.com/sj14/astral v0.1.2
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.0
)
